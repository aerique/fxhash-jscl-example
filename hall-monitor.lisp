;;;; hall-monitor.lisp

;;; Packages

(load "3rd-party/jscl-git/jscl.lisp")

(in-package :jscl)


;;; Functions

(defun hall-monitor (out-js &rest files)
  (loop with write-dates = (make-hash-table :test #'equal)
        for compile = (loop with write-date-changed = nil
                            for f in files
                            for old-write-date = (gethash f write-dates)
                            for new-write-date = (file-write-date f)
                            do (when (or (null old-write-date)
                                         (/= new-write-date old-write-date))
                                 (setf write-date-changed t
                                       (gethash f write-dates) new-write-date))
                            finally (return write-date-changed))
        do (when compile
             (format t "--- ~A ---~%" (get-universal-time))
             (compile-application files out-js))
           (sleep 1)))

