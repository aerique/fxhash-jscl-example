;;;; serve-local-directory.lisp
;;;;
;;;; Assumes Quicklisp is installed.

(format t "Loading Hunchentoot... ")
(force-output)
(ql:quickload :hunchentoot :silent t)
(format t "done.~%")

(let* ((acceptor (make-instance 'hunchentoot:easy-acceptor
                                :document-root *default-pathname-defaults*
                                :port 8000))
       (port (hunchentoot:acceptor-port acceptor))
       (host (if (null (hunchentoot:acceptor-address acceptor))
                 "0.0.0.0"
                 (hunchentoot:acceptor-address acceptor))))
  (hunchentoot:start acceptor)
  (format t "Serving HTTP on ~A port ~D (http://~A:~D/) ...~%"
          host port host port))
