# Makefile

.PHONY: watch clean distclean serve archive release

watch:
	sbcl --noinform --load hall-monitor.lisp --eval '(in-package :jscl)' --eval '(bootstrap)' --eval '(hall-monitor "app.js" "main.lisp")'

compile: main.lisp
	sbcl --noinform --load 3rd-party/jscl-git/jscl.lisp --eval '(in-package :jscl)' --eval '(bootstrap)' --eval '(compile-application (list "main.lisp") "app.js")' --eval '(cl-user::quit)'

serve: compile
	cp --update 3rd-party/jscl-git/jscl.js .
	sbcl --noinform --load resources/serve-current-directory.lisp

clean:
	rm -f app.js jscl.js

distclean: clean
	rm -f project.zip

archive:
	tar cvfj ../jscl-example-`date +"%Y%m%d%H%M"`.tar.bz2 .

release:
	zip project.zip LICENSE.md index.html style.css fxhash.js jscl.js app.js
