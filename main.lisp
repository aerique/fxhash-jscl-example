;;;; main.lisp
;;;;
;;;; This code is covered under the license as described in `LICENSE.md`.

;; Try this to clean up all the JSCL debug messages.
;(#j:console:clear)

;;; Constants

(defvar +2pi+  (* pi 2))


;;; Globals

(defparameter *release* nil)

(defparameter *animation-frame* nil)

(defparameter *canvas* (#j:document:getElementById "canvas"))
(defparameter *context* ((oget *canvas* "getContext") "2d"))

(defparameter *w* 1000)
(defparameter *h* 1000)

(defparameter *example1* 0)
(defparameter *example2* 0)


;;; Functions

;;; Common Functions

(defun fxrand (&optional maximum)
  (if maximum
      (* (#j:$fx:rand) maximum)
      (#j:$fx:rand)))


(defun log (string)
  (#j:console:log string))


(defun mkstr (&rest args)
  (with-output-to-string (s)
    (dolist (a args) (princ a s))))


;;; Color Functions

; https://gist.github.com/mjackson/5311256
;;
;; For license information see the "hsv2rgb" section in
;; [LICENSE.md](./LICENSE.md).
;;
;; `h`, `s` and `v` should be between 0 and 1
(defun hsv2rgb (h s v)
  (let* ((i (floor (* h 6)))
         (f (- (* h 6) i))
         (p (* v (- 1 s)))
         (q (* v (- 1 (* f s))))
         (t (* v (- 1 (* (- 1 f) s)))))
    (case (mod i 6)
      (0 `(,(round (* v 255)) ,(round (* t 255)) ,(round (* p 255))))
      (1 `(,(round (* q 255)) ,(round (* v 255)) ,(round (* p 255))))
      (2 `(,(round (* p 255)) ,(round (* v 255)) ,(round (* t 255))))
      (3 `(,(round (* p 255)) ,(round (* q 255)) ,(round (* v 255))))
      (4 `(,(round (* t 255)) ,(round (* p 255)) ,(round (* v 255))))
      (5 `(,(round (* v 255)) ,(round (* p 255)) ,(round (* q 255)))))))


(defun rgb2html (rgb)
  ;; `~X` for `format` is not implemented in JSCL, so ... :-\
  (let ((r ((oget (elt rgb 0) "toString") 16))
        (g ((oget (elt rgb 1) "toString") 16))
        (b ((oget (elt rgb 2) "toString") 16)))
    (format nil "#~A~A~A~A~A~A"
            (if (= 1 (length r)) "0" "")
            r
            (if (= 1 (length g)) "0" "")
            g
            (if (= 1 (length b)) "0" "")
            b)))


(defun random-color ()
  (rgb2html (hsv2rgb (fxrand) (fxrand) (fxrand))))


;;; Canvas Functions

;; `stub` is here because of the `funcall` in `draw-shapes`
(defun draw-circle (x y r stub c)
  (declare (ignore stub))
  (setf (oget *context* "fillStyle") c)
  ((oget *context* "beginPath"))
  ((oget *context* "arc") x y r 0 +2pi+)
  ((oget *context* "fill"))
  ((oget *context* "closePath")))


;; `x2` and `y2` are `w` and `h` in `draw-shapes`
(defun draw-line (x1 y1 x2 y2 c)
  (setf (oget *context* "strokeStyle") c)
  ((oget *context* "beginPath"))
  ((oget *context* "moveTo") x1 y1)
  ((oget *context* "lineTo") x2 y2)
  ((oget *context* "stroke"))
  ((oget *context* "closePath")))


(defun draw-rectangle (x y h w c)
  (setf (oget *context* "fillStyle") c)
  ((oget *context* "fillRect") x y w h))


(defun draw-shapes (&rest args)
  (loop with shapes = '(draw-circle draw-line draw-rectangle)
        repeat (ceiling (fxrand 32))
        ;; Grab a random shape from `shapes` (which are actually the names
        ;; of the functions).
        for shape = (elt shapes (floor (fxrand (length shapes))))
        for x = (fxrand *w*)
        for y = (fxrand *h*)
        ;; How `w` and `h` are determined is not totally random (only
        ;; indirectly) but dependent on `x` and `y` and is a way to guide a
        ;; generative art piece.
        for w = (* x 0.2)
        for h = (* y 0.2)
        for c = (random-color)
        do (funcall shape x y w h c))
  ;; I've put this here but it is really dependent on your own project when
  ;; to call this.
  (when #j:$fx:isPreview
    (#j:$fx:preview)))


;;; Resizing Functions

(defun set-canvas-width-and-height ()
  (let ((w #j:window:innerWidth)
        (h #j:window:innerHeight))
    (setf (oget *canvas* "width")  w
          (oget *canvas* "height") h
          *w* w
          *h* h)))


(defun resize-canvas (&rest args)
  (declare (ignore args))
  ;; This is not ideal. We should have RESIZE-CANVAS not call
  ;; `requestAnimationFrame`.
  (#j:window:cancelAnimationFrame *animation-frame*)
  (set-canvas-width-and-height)
  (setf (oget *context* "fillStyle") (random-color))
  ((oget *context* "fillRect") 0 0 *w* *h*)
  ;; Per W3C recommendation these should match the display refresh rate.
  (setf *animation-frame* (#j:window:requestAnimationFrame #'draw-shapes)))


(defun set-fxhash-features (event)
  (declare (ignore event))
  (#j:$fx:features
   (#j:JSON:parse (format nil "{ \"example1\": \"~D\",
                                 \"example2\": \"~D\" }"
                          *example1*
                          *example2*))))


;;; Main Program

(defun main ()
  (unless *release*
    (log (format nil "hash: ~S" #j:$fx:hash)))
  (setf *example1* (ceiling (fxrand  69)))
  (setf *example2* (ceiling (fxrand 420)))
  ;; Calculate your fxhash features here.
  (#j:window:addEventListener "load" #'set-fxhash-features)
  (setf #j:window:onresize #'resize-canvas)
  (log ",--------")
  (log "| THIS IS AN UNMODIFIED EXAMPLE PROJECT!")
  (log "| please see: https://codeberg.org/aerique/fxhash-jscl-example")
  (log "`--------")
  (resize-canvas))

(main)
