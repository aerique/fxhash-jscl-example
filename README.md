# fxhash Example Project for Common Lisp

The canonical home page of this project is https://codeberg.org/aerique/fxhash-jscl-example

(This project is also pushed to
 [GitLab](https://gitlab.com/aerique/fxhash-jscl-example)
 and
 [Sourcehut](https://git.sr.ht/~aerique/fxhash-jscl-example.git)
 but those sites are not monitored for support.)

An earlier version of this project was used for this piece on fxhash:
https://www.fxhash.xyz/generative/10665

<p align="center">
  <img src="resources/2022-04-08_10-06.png">
</p>

## Important: Read This Section First

This is an example project to get you started on fxhash.

However, you cannot just compile the code and submit it as a project to
fxhash, since it does not conform to all the required rules for submitting a
project. Specifically the output does not stay the same when the canvas is
resized.

Additionally it outputs a message to the console saying this is an
unmodified example project.

Also it's just a bunch of colored, randomly placed circles, lines and
rectangles. Not very original.

Please read the following guides first before submitting something to
fxhash:

- https://www.fxhash.xyz/doc/artist/guide-publish-generative-token
- https://www.fxhash.xyz/doc/artist/code-of-conduct

## Usage

- `git submodule update --init --recursive`

Then:

- `make compile`
- `make serve`
    - or `python -m http.server`

(See the `Makefile` for the actual commands.)

Ideally you'd run `make watch` in one terminal and `make serve` in another.
This way you'll have live recompilation when saving a source file and live
reloading in the browser of your changes. It doesn't work as fluid as in
other languages yet (Ruby, Node, ClojureScript) but it's a first attempt.

Once you start adding more Lisp files besides `main.lisp` you'll need to add
those to the Makefile. Basically just search where `main.lisp` is used and
add it there.

## Description

With the warnings from the first section out of the way: this project should
let you hit the ground running if you're interested in publishing on fxhash
using Common Lisp. All that's left to do is to just draw the fucking owl.

A real high-level overview:

- `main` is called first
- `resize-canvas` is called
- `draw-shapes` is called
- `draw-shapes` has all the logic for drawing the circles, lines and rectangles

If you want animations you need to keep calling
`(#j:window:requestAnimationFrame #'draw-shapes)` at the end of
`draw-shapes`.

## Notes on JSCL

`(oget *context* ...` stuff should really be put into a macro, but for now
pay attention that even for canvas calls (or any `oget` call?) that do not
take any arguments you still need to wrap them into an extra set of
parenthesis, otherwise they will not be called as a function.

So in the source we have:

    ((oget *context* "fillRect") x y w h))

but `beginPath` and others also need to look like this:

    ((oget *context* "beginPath"))

<p align="center">
  <img src="resources/2022-04-08_10-04.png">
</p>
